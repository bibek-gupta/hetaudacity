import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs';
import { Observable } from 'rxjs';

@Injectable()
export class ClientService {

  constructor(private http: Http) { }
  register(data) {
    let headers = new Headers({ 'Content-Type': 'application/json' })
    let options = new RequestOptions({ headers: headers, withCredentials: true })
    return this.http.post('http://localhost:8000/api/auth/register', data, options)
      .map(this.extractData).catch(this.handleError)
  }

  getCategory() {
    let headers = new Headers({ 'Content-Type': 'application/json' })
    let options = new RequestOptions({ headers: headers, withCredentials: true })
    return this.http.get('http://localhost:8000/api/client/getCategory', options)
      .map(this.extractData).catch(this.handleError)
  }

  getProfileInformation() {
    let data = { id: localStorage.getItem('navigationId') }
    let headers = new Headers({ 'Content-Type': 'application/json' })
    let options = new RequestOptions({ headers: headers, withCredentials: true })
    return this.http.post('http://localhost:8000/api/client/getProfileInformation', data, options)
      .map(this.extractData)
      .catch(this.handleError)
  }



  // getProfileDetails(info){
  getProfileDetails() {
    let data = { id: localStorage.getItem('indexForProfileInformation') }
    console.log(data)
    let token=  localStorage.getItem('access_token');
    let headers = new Headers({ 'Content-Type': 'application/json',
    'Accept' : 'application/json',
    'Authorization' : 'Bearer '+ token,
    });
    let options = new RequestOptions({ headers: headers, withCredentials: true })
    return this.http.post('http://localhost:8000/api/client/getProfileDetails', data, options)
      .map(this.extractData)
      .catch(this.handleError)
  }

  getListOfBuilding(){
    let headers = new Headers({ 'Content-Type': 'application/json' })
    let options = new RequestOptions({ headers: headers, withCredentials: true })
    return this.http.get('http://localhost:8000/api/client/getListOfBuildings', options)
    .map(this.extractData)
    .catch(this.handleError)


  }
//search
search(data){
  let headers = new Headers({ 'Content-Type': 'application/json', 'Accept' : 'application/json'});
  let options = new RequestOptions({ headers: headers, withCredentials: true })
  return this.http.get('http://localhost:8000/api/client/search/'+data,options)
  .map(this.extractData)
  .catch(this.handleError)

}

  //feedback and review

  postFeedBack(data){
    let token=  localStorage.getItem('access_token');
    let headers = new Headers({ 'Content-Type': 'application/json', 'Accept' : 'application/json','Authorization' : 'Bearer '+ token});
    let options = new RequestOptions({ headers: headers, withCredentials: true })
    return this.http.post('http://localhost:8000/api/client/postFeedBack',data,options)
                .map(this.extractData)
                .catch(this.handleError)
  }

  private extractData(res: Response) {
    let response = res.json();
    return response || {}

  }
  private handleError(error: Response | any) {
    return Observable.throw(error)
  }
}
