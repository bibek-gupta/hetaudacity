import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {RouterModule,Routes} from '@angular/router';
import { menuComponent } from './client/menu/menu.component';
import {FooterComponent} from './client/footer/footer.component';
import {ViewsModule} from './client/views.module';
import {AdminModule} from './admin/admin.module';
import { ViewsComponent } from './client/views.component';
import {ReactiveFormsModule} from '@angular/forms';
import {LoginService} from '../app/client/logindemo/login.service';
import {UserService} from '../app/client//logindemo/user.service';
import{AdminLoginService} from '../app/admin/adminlogin/admin-login.service';
import { AdminComponent } from './admin/admin.component';
import{AdminloginComponent} from '../app/admin/adminlogin/adminlogin.component'

const routes:Routes=[
  { path:'home',  component:ViewsComponent },
  {path:'', redirectTo :'home' ,pathMatch:'full'},
  {
    path:'admin',component:AdminComponent},
  {path:'admin/login',component:AdminloginComponent},

  { path: '**', redirectTo: 'home', pathMatch: 'full' },


];
@NgModule({
  declarations: [
    AdminloginComponent,
    AppComponent,
    ViewsComponent,
    menuComponent,
    FooterComponent,
    
    

  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    ReactiveFormsModule,
    ViewsModule,
    AdminModule
    // AngularWebStorageModule
    

  ],
  providers: [LoginService,UserService,AdminLoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
