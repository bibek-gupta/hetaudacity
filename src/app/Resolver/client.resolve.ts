import { Injectable } from "../../../node_modules/@angular/core";
import { Resolve, Router, ActivatedRoute } from "../../../node_modules/@angular/router";
import { ClientService } from '../clientservice/client.service'
import { Observable } from "../../../node_modules/rxjs";
import { Route } from "../../../node_modules/@angular/compiler/src/core";
import { ENV } from "../env";


@Injectable()

export class ClientResolver implements Resolve<any>    {

    constructor(private clientservice: ClientService, private routerparams: ActivatedRoute, private router: Router) {
       
    }

    resolve() {
        
        return this.clientservice.getProfileDetails()
            .catch(
                (error) => {
                    this.router.navigate(['/home/login']);
                    return Observable.of(null);
                }
            );
    }

}

@Injectable()
export class CategoryResolver implements Resolve<any>    {
    constructor(private clientservice: ClientService,private routerparams: ActivatedRoute, private router: Router) { }

    resolve() {
        return this.clientservice.getProfileInformation()
            .catch(
                () => {
                    return Observable.of('no data Available for the moment');
                }
            );
    }

}