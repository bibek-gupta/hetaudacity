import { Component, OnInit } from '@angular/core';
import { AdminLoginService } from './admin-login.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.css']
})
export class AdminloginComponent implements OnInit {
  adminLoginForm: FormGroup
  constructor(private service: AdminLoginService, private fb: FormBuilder, private router:Router) { }
  // 
  status
  ngOnInit() {
    this.adminLoginForm = this.fb.group({
      email: 'Enter Your User Name',
      password: 'Enter Your password'
    })

  }
  adminLogin() {
    let adminCredentials = this.adminLoginForm.value;

      this.service.adminLogin(adminCredentials).subscribe(
        (response)=>{
          console.log(response)
          localStorage.setItem('admin_token',response.access_token)
          this.router.navigate(["admin"])
        },
        (error)=>{
          console.log(error)
          this.status= error.status
          // let message = JSON.parse(error._body)
          // this.emailError=message.errors.email[0]
          // console.log(this.emailError)
          // this.passwordError = message.errors.password[0]
          // console.log(this.passwordError)
        }
      )
  }
}

