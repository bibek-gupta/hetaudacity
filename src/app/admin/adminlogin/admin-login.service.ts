import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import 'rxjs';
import { Observable } from 'rxjs';
@Injectable()
export class AdminLoginService {

  constructor(private http:Http) { }


  adminLogin(data){
    let headers = new Headers({ 'Content-Type': 'application/json' })
    let options = new RequestOptions({ headers: headers, withCredentials: true })
    return this.http.post('http://localhost:8000/api/admin/login',data,options)
              .map(this.extractData)
              .catch(this.handleError)

  }
  
  private extractData(res: Response) {
    let response = res.json();
    return response || {}

  }
  private handleError(error: Response | any) {
    return Observable.throw(error)
  }

}
