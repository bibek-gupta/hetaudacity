import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
// import {AdminAuthService} from '../admin/adminAuth.service'
import { ENV } from "../env";

@Injectable()
export class AdminGuard implements CanActivate {
  constructor( private router: Router) { }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    ENV.setAdminAccessToken()
    
    if (ENV.setAdminAccessToken()) {
      return true;
    } else {
       this.router.navigate(['admin/login']);
      return false;
    }
    
  }



}
