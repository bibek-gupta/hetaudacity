import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import { AdminComponent } from './admin.component';
import { RouterModule, Routes } from '@angular/router';
import {AdminService} from './admin.service'
import {AdminGuard} from '../admin/admin.guard';





const routesSub: Routes = [
  {      
    canActivate:[AdminGuard],
    path: 'admin', component: AdminComponent,

    children: [
      // {
      //   path:'login',component:AdminloginComponent
      // },
      // {path:'**', redirectTo :'admin'},



    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routesSub),

  ],
  declarations: [
    // AdminComponent,
  
    // AdminloginComponent
  ],
  providers:[AdminService,AdminGuard]
})
export class AdminModule { }
