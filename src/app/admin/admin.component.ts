import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AdminService } from './admin.service'
declare var jQuery: any;
declare var $;
declare var reader;

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  categoryForm: FormGroup
  imageUpload: FormGroup
  informationForm: FormGroup
  data = []
  buildings = []
  infoLists = []
  viewinformation = []
  categoryId
  public isActive = 0
  public files
  categoryLists = []
  indexForEdit
  indexForDelete
  responsemsg
  imageChangedEvent: any = ""
  croppedImage: any = ''
  cropperReady
  imageUploadStatus
  constructor(private fb: FormBuilder, private adminservice: AdminService) {

    jQuery('#addinformation').hide()

    //category form
    this.categoryForm = this.fb.group({
      category: ''
    })
    //informationform

    this.informationForm = this.fb.group({
      updateId: '',
      categoryId: '',
      name: '',
      email: '',
      url: '',
      phone: '',
      address: '',
      latitude: '',
      longitude: '',
      services: '',
      description: '',
      contact_person: '',
      contact_person_number: ''
    })


    this.imageUpload = this.fb.group({
      categoryId: '',
      mainImage: false,
      buildings: ''
    })
  }

  ngOnInit() {
    jQuery('#manageCategories').hide()
    jQuery('#addinformation').hide()
    jQuery('#uploadImage').hide()
    this.adminservice.getCategory().subscribe(
      (response) => {
        // console.log(response)
        this.categoryLists = response.category;
        this.categoryId = this.categoryLists[0].id
        console.log(this.categoryId)
        // this.categoryLists.push(response.category) 
        this.adminservice.getInformation(this.categoryId).subscribe(
          (response) => {
            console.log(response)
            this.infoLists = response.information
          }
        )
      }
    )
  }

  //calls the dashboard view to the admin panel
  dashBoard() {
    jQuery('#addinformation').hide()
    jQuery('#manageCategories').hide()
    jQuery('#uploadImage').hide()
    jQuery('#tabbedCategories').show()


  }

  //displays a modal to add category 
  addCategory() {
    jQuery('#categoryModal').modal('show')
  }


  // displays forms for adding information to the related category
  addInformation() {
    // this.show=true;

    jQuery('#manageCategories').hide()
    jQuery('#uploadImage').hide()
    jQuery('#tabbedCategories').hide()
    jQuery('#addinformation').show()

  }
  //managing categories
  manageCategory() {
    jQuery('#uploadImage').hide()
    jQuery('#addinformation').hide()
    jQuery('#tabbedCategories').hide()
    jQuery('#manageCategories').show()
  }
  //adding images
  addImage() {
    jQuery('#tabbedCategories').hide()
    jQuery('#manageCategories').hide()
    jQuery('#addinformation').hide()
    jQuery('#image-preview').hide()
    jQuery('#uploadImage').show()
  }
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(image: string) {
    this.croppedImage = image;
  }
  imageLoaded() {
    // show cropper
  }
  loadImageFailed() {
    // show message
  }

  //calling service to store image in the db
  uploadImage() {
    let imageData = {};
    imageData = this.imageUpload.value
    imageData['image']= this.croppedImage
    console.log( imageData)
    this.adminservice.uploadImage(imageData).subscribe(
      (response) => {
        // this.imageUpload.reset();
        console.log(response)
        this.imageUploadStatus=response.message;
      }
    )
  }

  //post category information to the the database

  postCategoryData() {
    jQuery('#categoryModal').modal('hide')
    this.data = this.categoryForm.value
    console.log(this.categoryForm.value)
    this.adminservice.postCategoryData(this.data).subscribe(
      (response) => {
        console.log(response)
        this.categoryLists.push(response.category);
      }
    )
  }

  //hides and show the category tab to display information

  tabForCategoryInformation(i, category, id) {
    this.isActive = i;
    // console.log(i);
    console.log(category)
    this.adminservice.getInformation(id).subscribe(
      (response) => {
        console.log(response)
        this.infoLists = response.information
        console.log(this.infoLists)
      }
    )
  }

  //post category infomation to the database


  postInformation() {
    this.data = this.informationForm.value
    this.adminservice.postInformation(this.data).subscribe(
      (response) => {
        console.log(response.information)
        if (response) {
          this.infoLists.push(response.information)
          this.responsemsg = response.status
          console.log(this.responsemsg)
          this.informationForm.reset
        }
      },
      (error) => {
        console.log(error)
      }
    )
  }

  //view detail information 
  viewInformation(id, i) {
    console.log(id)
    console.log(i)
    console.log(this.infoLists[i])
    this.viewinformation = this.infoLists[i]
    jQuery('#viewInformation').modal('show')
  }

  //edit the information
  editInformation(i) {
    jQuery('#updateInformation').modal('show')
    this.indexForEdit = i
    this.informationForm.controls['updateId'].setValue(this.infoLists[i].id)
    this.informationForm.controls['categoryId'].setValue(this.infoLists[i].categoryId)
    this.informationForm.controls['name'].setValue(this.infoLists[i].name);
    this.informationForm.controls['email'].setValue(this.infoLists[i].email);
    this.informationForm.controls['url'].setValue(this.infoLists[i].url);
    this.informationForm.controls['phone'].setValue(this.infoLists[i].phone);
    this.informationForm.controls['address'].setValue(this.infoLists[i].address);
    this.informationForm.controls['contact_person'].setValue(this.infoLists[i].contact_person);
    this.informationForm.controls['contact_person_number'].setValue(this.infoLists[i].contact_person_number);
    this.informationForm.controls['services'].setValue(this.infoLists[i].services);
    this.informationForm.controls['description'].setValue(this.infoLists[i].description);
    this.informationForm.controls['latitude'].setValue(this.infoLists[i].latitude);
    this.informationForm.controls['longitude'].setValue(this.infoLists[i].longitude);
  }

  //update the edited information
  updateInformation() {
    this.data = this.informationForm.value
    console.log(this.informationForm.value)
    this.adminservice.updateInformation(this.informationForm.value).subscribe(
      (response) => {
        console.log(response)
        this.infoLists[this.indexForEdit] = response.information
        jQuery('#updateInformation').modal('hide')
      },
      (error) => {
        console.log(error)
      }
    )
  }



  //delete the information


  modalForDelete(id) {
    jQuery('#deleteModal').modal('show')
    this.indexForDelete = id;
    console.log(this.indexForDelete)
  }
  deleteInformation() {
    jQuery('#deleteModal').modal('hide')
    this.adminservice.deleteInformation(this.indexForDelete).subscribe(
      (response) => {
        console.log(this.indexForDelete)
        for (let x in this.infoLists) {
          if (this.infoLists[x].id == this.indexForDelete) {
            this.infoLists.splice(parseInt(x), 1)
          }
        }
        console.log(response)
      }
    )
  }


  selectme(id) {
    this.adminservice.getInformation(id).subscribe(
      (response) => {
        console.log(response)
        this.buildings = response.information
      }, (error) => {
        console.log(error)
      }
    )
  }
}

















