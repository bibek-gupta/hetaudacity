import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import 'rxjs';
import { Observable } from 'rxjs';

@Injectable()
export class AdminService {
 id=""
  constructor(private http: Http) { }


  postCategoryData(data) {
    let headers = new Headers({ 'Content-Type': 'application/json' })
    let options = new RequestOptions({ headers: headers, withCredentials: true })
    return this.http.post('http://localhost:8000/api/admin/addcategory', data, options)
      .map(this.extractData)
      .catch(this.handleError)
  }
  getCategory() {
    let headers = new Headers({ 'Content-Type': 'application/json' })
    let options = new RequestOptions({ headers: headers, withCredentials: true })
    return this.http.get("http://localhost:8000/api/admin/getcategory", options)
      .map(this.extractData)
      .catch(this.handleError)
  }

  getInformation(id) {
    let headers = new Headers({ 'Content-Type': 'application/json' })
    let options = new RequestOptions({ headers: headers, withCredentials: true })
    return this.http.get("http://localhost:8000/api/admin/getInformation/" + id, options)
      .map(this.extractData)
      .catch(this.handleError)
  }


  // methods to handle categories information


  postInformation(data) {
    let headers = new Headers({ 'Content-Type': 'application/json' })
    let options = new RequestOptions({ headers: headers, withCredentials: true })
    return this.http.post('http://localhost:8000/api/admin/postInformation', data, options)
      .map(this.extractData).catch(this.handleError)
  }

  updateInformation(data){
    let headers = new Headers({ 'Content-Type': 'application/json' })
    let options = new RequestOptions({ headers: headers, withCredentials: true })
    return this.http.post('http://localhost:8000/api/admin/updateInformation', data, options)
      .map(this.extractData).catch(this.handleError)
  }

  deleteInformation(info){
    let data={id:info}
    let headers= new Headers({'Content-Type':'application/json'})
    let options= new RequestOptions({headers:headers,withCredentials:true})
    return this.http.post('http://localhost:8000/api/admin/deleteInformation',data,options)
  }





  uploadImage(image){
    let header= new Headers({'Content-Type':'application/json'})
    let options=new RequestOptions({headers:header,withCredentials:true})
    return this.http.post('http://localhost:8000/api/admin/uploadImage',image,options)
                    .map(this.extractData)
                    .catch(this.handleError)

  }



  private extractData(res: Response) {
    let response = res.json();
    return response || {}

  }
  private handleError(error: Response | any) {
    return Observable.throw(error)
  }
}
