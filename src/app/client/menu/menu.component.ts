import { Component, OnInit, Output } from '@angular/core';
import {ClientService} from '../../clientservice/client.service'
import {ENV} from '../../env'
import { Router } from '../../../../node_modules/@angular/router';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class menuComponent implements OnInit {
categories=[]
flag=false
loggedInStatus=""
constructor(private clientservice:ClientService,private router:Router) { }
  ngOnInit() {
    this.loggedInStatus= localStorage.getItem('access_token')
    if(this.loggedInStatus){
      this.flag=true
    }else{
      this.flag=false
    }
    console.log(this.flag)
    console.log(this.loggedInStatus)
    this.clientservice.getCategory().subscribe(
      (response)=>{
        console.log(response)
        this.categories=response.category
      },
      (error)=>{
        console.log(error)
      }
    )
  }

  getNavigationId(id,category){
    localStorage.setItem('navigationId',id)
    localStorage.setItem('navigationName',category)
    ENV.categoryUrl=category
    console.log(ENV.categoryUrl)
    ENV.navigationId=id
    console.log(ENV.navigationId)
  }
  Logout(){
    localStorage.clear();
    this.router.navigate(["home"])
    this.loggedInStatus= localStorage.getItem('access_token')
    if(this.loggedInStatus){
      this.flag=true
    }else{
      this.flag=false
    }
  }
}
