import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../clientservice/client.service'
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '../../../../node_modules/@angular/forms';




declare let google: any;

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.css'],

})
export class ProfileDetailsComponent implements OnInit {
  imagesLists = []
  mainImage
  profileDetails: {}
  serviceArray = []
  latitude: number
  longitude: number
  currenturl: string = "home"
  public feedBackForm: FormGroup
  public ratedStar: number
  public reviewStatus: boolean = false
  public profileId
  ratedStarArray: any[]
  feedBacks = []
  userFeedBacks = []
  username = []
  imagesList = []
  public ratedflag: boolean = false
  public lat: Number = 24.799448
  public lng: Number = 120.979021

  public origin: any
  public destination: any

  constructor(
    private clientservice: ClientService,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {

  }


  ngOnInit() {

    this.feedBackForm = this.fb.group(
      {
        feedback_title: ['', Validators.required],
        feedback: ['', Validators.required]
      }
    )
    this.profileDetails = this.route.snapshot.data.response.details
    this.feedBacks = this.route.snapshot.data.response.feedback
    this.username = this.route.snapshot.data.response.userDetails
    this.userFeedBacks = this.route.snapshot.data.response.userfeedback
    for (let x in this.route.snapshot.data.response.images) {
      this.imagesLists.push(this.route.snapshot.data.response.images[x]);
    }

    this.mainImage = this.route.snapshot.data.response.mainImage[0].image
    this.serviceArray = this.profileDetails[0].services.split(",")
    this.profileId = this.profileDetails[0].id
    this.latitude = this.profileDetails[0].latitude
    this.longitude = this.profileDetails[0].longitude
    if (this.userFeedBacks.length > 0) {
      this.ratedStarArray = Array(this.userFeedBacks[0].ratedStar).fill('dummy')
    }

  }
  getRatingStar(star) {
    this.ratedStar = star
    this.reviewStatus = true
  }


  postFeedBack() {
    let data = {}
    data = this.feedBackForm.value;
    data['star'] = this.ratedStar;
    data['profileId'] = this.profileId
    this.clientservice.postFeedBack(data).subscribe(
      (response) => {
        this.feedBackForm.reset()
        this.feedBacks.push(response.feedback)
        this.ratedflag = response.status
      },
      (error) => {
        console.log(error)
      }
    )
  }



  locateMe() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.showPosition(position);
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }


  showPosition(position) {
    let prevlat = this.latitude
    let prevlong = this.longitude
    let curlat = position.coords.latitude;
    let curlong = position.coords.longitude;
    //  this. getDirection(prevlat,prevlong,curlat,curlong)

    // this.origin = { lat: prevlat, lng: prevlong }
    // this.destination = { lat: curlat, lng: curlong }
    this.origin = { lat: 24.799448, lng: 120.979021 }
    this.destination = { lat: 24.799524, lng: 120.975017 }
    console.log(this.origin)
    console.log(this.destination)
    // this.latitude = position.coords.latitude;
    // this.longitude = position.coords.longitude;

  }

  // getDirection(prevlat, prevlong, curlat, curlong) {

  //   this.origin = { lat: prevlat, lng: prevlong }
  //   this.destination = { lat: curlat, lng: curlong }
  //   console.log(this.origin, this.destination)


  // }

}
