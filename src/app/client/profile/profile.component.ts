import { Component, OnInit } from '@angular/core';
import { ENV } from '../../env'
import { ClientService } from '../../clientservice/client.service'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profiles = []
  profileImages = []
  categoryname=localStorage.getItem('navigationName')

  constructor(private clientservice: ClientService,private route: ActivatedRoute ) { }

  ngOnInit() {
    this.profiles = this.route.snapshot.data.response.profileInformation
    this.profileImages = this.route.snapshot.data.response.profileImages
    

    for(let i=0; i<this.profileImages.length; i++){
      this.profiles[i].images = this.profileImages[i].image
    }
    // console.log('your data is here good to go', this.profiles)
    // console.log('your profile pics are',this.profileImages)

  }




  setRoute(route, id) {
    localStorage.setItem('indexForProfileInformation',id)
    console.log(id)
    ENV.routeurl = route
    console.log(ENV.routeurl)
    localStorage.setItem('currentUrl',route)
  }

}

