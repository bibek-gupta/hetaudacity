import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../clientservice/client.service'
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import { ENV } from '../../env'
declare var jQuery: any;
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  listOfBuildings = []
  categoriesList = []
  images = []
  flag: boolean = false
  searchText
  constructor(private service: ClientService) { }

  ngOnInit() {

    jQuery('.responsive').slick({
      dots: true,
      infinite: true,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });


    this.service.getListOfBuilding().subscribe(
      (response) => {
        this.listOfBuildings = response.listOfBuildings
        this.categoriesList = response.categoriesList
        this.images = response.images
        console.log('the images are', this.images)

        for (let i = 0; i < response.images.length; i++) {
          this.categoriesList[i].images = this.images[i].image
        }
        // console.log(this.listOfBuildings)
        console.log(this.categoriesList)
      }
    )


    // this.searchText = new FormControl();
    // this.searchText.valueChanges
    //   .debounceTime(400)
    //   .distinctUntilChanged()
    //   .subscribe(term => {
    //     if (term) {
    //       console.log(term)
    //       this.search(term).subscribe();
    //       this.flag = true
    //     } else {
    //       this.flag = false
    //     }
    //   });
  }
  setCurrentUrl(visitedUrl,id){
    console.log('your indexForInformation',id)
    console.log('you are visiting',visitedUrl)
    localStorage.setItem('indexForProfileInformation',id)
    localStorage.setItem('currentUrl',visitedUrl)
  }


  onSearchChange(searchValue: string) {
    console.log(searchValue);
    if (!searchValue) {
      this.flag = false
    } else {
      this.flag = true

    }
  }
  getCategory(id, category) {
    localStorage.setItem('navigationId', id)
    localStorage.setItem('navigationName', category)
    ENV.categoryUrl = category
    console.log(ENV.categoryUrl)
    ENV.navigationId = id
    console.log(ENV.navigationId)

  }

}
