import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { HomepageComponent } from './homepage/homepage.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminComponent } from '../admin/admin.component';
import { AgmCoreModule } from '@agm/core';
import { ClientService } from '../clientservice/client.service'
import {LoginService} from './logindemo/login.service'
import { ViewsComponent } from './views.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './logindemo/login.component';
import { ProfileComponent } from './profile/profile.component'
import { ProfileDetailsComponent } from './profile-details/profile-details.component'
import { ENV } from '../env'
import { ClientResolver } from '../Resolver/client.resolve'
import { CategoryResolver } from '../Resolver/client.resolve';
import { RegisterComponent } from './register/register.component'
import {LoginGuard} from './logindemo/login.guard'
import { FilterPipe } from './homepage/filter.pipe';
import { ImageCropperModule } from 'ngx-image-cropper';
import { AgmDirectionModule } from 'agm-direction'



const routesSub: Routes = [
  {
    path: 'home', component: ViewsComponent,

    children: [

      {
        path: 'view', component: HomepageComponent, data: { state: 'home' },
        resolve: { response: CategoryResolver }
      },
      {
        path: 'category/:' + ENV.categoryUrl,
        component: ProfileComponent, data: { state: ENV.categoryUrl },
        resolve: { response: CategoryResolver }, 
      },
      
      
      {  path: 'login', component: LoginComponent, data: { state: 'login' } },
      { path: 'register', component: RegisterComponent },

      {canActivate:[LoginGuard],
        path: ':' + ENV.routeurl,
        component: ProfileDetailsComponent, data: { state: ENV.routeurl },
        resolve: { response: ClientResolver }, 
      },

      { path: '**', redirectTo: 'view', pathMatch: 'full' },


    ]
  }
];

@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    ImageCropperModule,
    RouterModule.forRoot(routesSub),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCQkuGaHDCs2qHbxQ3s5lk4Crtm3BgCooE'
      // apiKey: 'AIzaSyDhhfQ2PFJbD6pYwfyTLwXPupN8aHZqMoQ'

    }),
    AgmDirectionModule
  ],
  declarations: [
    HomepageComponent,
    AdminComponent,
    LoginComponent,
    ProfileComponent,
    ProfileDetailsComponent,
    RegisterComponent,
    FilterPipe
  ],
  providers: [ClientService, ClientResolver, CategoryResolver,LoginService,LoginGuard],


})
export class ViewsModule {

}
