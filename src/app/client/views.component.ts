import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../client/router.animation';
@Component({
  animations: [ routerTransition ],
  selector: 'app-views',
  templateUrl: './views.component.html',
  styleUrls: ['./views.component.css']
})
export class ViewsComponent implements OnInit {
  constructor() { }

  ngOnInit() {
    
  }
  getState(outlet) {
    return outlet.activatedRouteData.state;
  }
}
