import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw'
import { ENV } from "../../env";
@Injectable()
export class LoginService {

  constructor(private http:Http) { }
  postLoginData(data){
    
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post('http://localhost:8000/api/auth/login',data,options)
      .map(this.extractData)
      .catch(this.handleError);
   }
   private extractData(res: Response) {
    let response = res.json();
    return response || {};
  }
  private handleError(error: Response | any) {
    return Observable.throw(error);
   }
}
