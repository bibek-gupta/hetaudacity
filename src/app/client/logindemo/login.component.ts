import { Component, Input, EventEmitter, Output, OnChanges, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { ENV } from '../../env'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public checked = false;
  public is_admin = false;
  public active = false;
  public user_type = false;
  public current_user = "value";



  constructor(private loginservice: LoginService, private router: Router) { }
  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl(),
      password: new FormControl(),
    });
  }

  submitLoginForm() {
    this.loginservice.postLoginData(this.loginForm.value).subscribe(
      (response) => {
        console.log(response)
        localStorage.setItem('access_token', response.access_token);
        if(localStorage.getItem('currentUrl')){
          this.router.navigate(["home/" + localStorage.getItem('currentUrl')])
          
        }else{
          this.router.navigate(['home/'])
        }
        ENV.setaccesToken();
      },
      (error) => {
        console.log(error)
      }
    )
  }
}
