import { Injectable,EventEmitter } from '@angular/core';

@Injectable()
export class UserService {

  constructor() { }
  private static loggedIn:boolean = false;
  public static setLoggedInStatus(status){
     UserService.loggedIn = true
  }
  public static getLoggedInStatus(){
    return UserService.loggedIn
    }


}
