import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from './user.service';
import { ENV } from "../../env";

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(private user: UserService, private router: Router) { }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    ENV.setaccesToken()
    if (UserService.getLoggedInStatus()) {
      return true;
    } else {
      this.router.navigate(['home/login']);
      return false;
    }
    
  }



}
