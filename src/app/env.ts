import { UserService } from '../app/client/logindemo/user.service';

export class ENV {
  public static loginFlag = false
  public static currentRouteUrl
  public static navigationId
  public static routeurl: string
  public static categoryUrl:string
  public static Request_URL = 'http://localhost:8000'
  public static accessToken
  public static email
  public static first_name
  public static middle_name
  public static last_name
  public static phone_number
  public static p_address
  public static c_address
  public static mobile_number

  public static setLoggedIn


  public static setaccesToken() {
    if (localStorage.getItem('access_token')) {
      UserService.setLoggedInStatus(true);
    } else {
      UserService.setLoggedInStatus(false);
    }
  }


  public static setAdminAccessToken(){
    if(localStorage.getItem('admin_token')){
       return  this.setLoggedIn=true;
    }else{
       return this.setLoggedIn=false;
    }
  }
}

